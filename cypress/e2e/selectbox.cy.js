describe('Select box', () => {
    it('Should pick an option from select box', () => {
        cy.visit('https://devexpress.github.io/testcafe/example/');
        cy.get('[data-testid="macos-radio"]').click();
        cy.get('[data-testid="macos-radio"]').should('have.value', 'MacOS');
    });
});