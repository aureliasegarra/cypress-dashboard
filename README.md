[![Language - Javascript](https://img.shields.io/badge/Javascript-F2DB3F?style=for-the-badge&logo=javascript&logoColor=white)](https://)
[![Language - Javascript](https://img.shields.io/badge/Jenkins-black?style=for-the-badge&logo=jenkins&logoColor=white)](https://)
[![cypress-jenkins](https://img.shields.io/endpoint?url=https://cloud.cypress.io/badge/detailed/xvuhgx/main&style=flat&logo=cypress)](https://cloud.cypress.io/projects/xvuhgx/runs)



<br/>
<br/>
<div align="center">
    <img src="./logo.png" alt="Logo" width="50%">
    <br/>
    <br/>
    <h1 align="center"><strong>Formation Cypress - CI</strong></h1>
    <br />
    <p align="center">Docker - Page Object - Actions - CI - Visual Regression</p>
</div>
  
 
<br/>
<br/>
